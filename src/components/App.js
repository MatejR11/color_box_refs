import { useRef, useEffect } from 'react';
import '../styles/App.css';

function App() {
  const boxDiv = useRef();

  useEffect(() => {
    boxDiv.current.style.width = '100px';
    boxDiv.current.style.height = '100px';
  }, []);

  function handleIncrementDivSize() {
    let width = parseInt(boxDiv.current.style.width, 10);
    let height = parseInt(boxDiv.current.style.height, 10);

    width += 10;
    height += 10;

    boxDiv.current.style.width = `${width}px`;
    boxDiv.current.style.height = `${height}px`;
  }

  function handleBackgroundColor(e) {
    boxDiv.current.style.backgroundColor = e.target.value;
  }

  function handleResetChanges() {
    boxDiv.current.style.width = '100px';
    boxDiv.current.style.height = '100px';
    boxDiv.current.style.backgroundColor = 'transparent';
  }

  return (
    <div className="App">
      <header className="App-header">
        <div
          ref={boxDiv}
          style={{
            marginLeft: '50%',
            marginRight: '50%',
            border: '1px solid black',
          }}
        />

        <button onClick={handleIncrementDivSize}>Increase box size</button>

        <select onChange={handleBackgroundColor}>
          <option value="transparent">Transparent</option>
          <option value="red">Red</option>
          <option value="yellow">Yellow</option>
          <option value="gray">Gray</option>
        </select>

        <button onClick={handleResetChanges}>Reset</button>
      </header>
    </div>
  );
}

export default App;
